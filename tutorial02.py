import random


def merge(*lists):
    new_list = []
    lists = list(lists)
    while lists:
        lists.sort(key=lambda x: x[0])
        new_list.append(lists[0].pop(0))
        if len(lists[0]) == 0:
            lists.pop(0)
    return new_list

def pyramid(n):
    for i in range(1, n+1):
        string = "".join([str(j) for j in range(1, i+1)])
        string += "".join([str(j) for j in range(1, i)][::-1])

        print(string.center(2*n-1))

def summation(n):
    result = 0
    for i in range(1, n+1):
        result += i
    return result

def sum_efficient(n):
    return (n+1)*n/2

def guess_number(n):

    number = random.randint(1, n)

    while True:
        x = int(input("Enter your guess: "))
        if x == number:
            print("You won!")
            break
        elif x < number:
            print("The secret number is bigger")
        else:
            print("The secret number is smaller")

def main():
    a = [1, 3, 6, 8, 10]
    b = [3, 3, 5, 7, 8]
    c = [1, 2, 2, 4, 12]

    print(merge(a, b, c))

    pyramid(9)

    guess_number(100)

if __name__ == "__main__":
    main()
