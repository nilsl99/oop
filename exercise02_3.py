import random
from collections import defaultdict

def load_words(filename="words.txt"):
    """
    This function loads all possible words from a txt file.
    Each word has to be on a seperate line.
    """
    with open(filename, "r") as file:
        return file.readlines()

def print_word(word, guesses):
    """
    This function will print the word to the standard output, but will exchange all non-guessed characters with an underscore.

    Parameters:
        word: The word to be printed
        guesses: A dictionary containing wether a letter as bin guessed or not.
    """
    for c in word:
        if guesses[c.upper()]:
            print(c, end="")
        else:
            print("_", end="")
    print("")

def main():
    # Load all words and select one at random
    words = load_words()
    life = 10
    word = random.choice(words).strip()
    guesses = defaultdict(bool)

    # Loop until the player runs out of lifes or guesses all letters
    while life > 0 and (False in [guesses[c.upper()] for c in word]):
        print_word(word, guesses)
        guess  = input("> ")

        if guess.upper() in word.upper():
            guesses[guess.upper()] = True
        else:
            life -= 1
            print(f"{guess.upper()} does not occur! You have {life} attempts left!")

    print(word)

    # Check wether the player has won or lost
    if life > 0:
        print("Yeehaw! You won!")
    else:
        print("Game over! You lost!")

if __name__ == "__main__":
    main()
