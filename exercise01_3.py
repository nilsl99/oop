import numpy as np
import matplotlib.pyplot as plt


def is_percolating_rec(G, curr_x, curr_y, visited=None):
    """
    Recursively checks if there is a percolation in a subgraph of the grid.

    Args:
        G: The grid.
        curr_x: The current x-coordinate.
        curr_y: The current y-coordinate.
        visited: A list of visited cells.

    Returns:
        True if there is a percolation, False otherwise.
    """
    if visited is None:
        visited = np.zeros(G.shape)
    visited[curr_x, curr_y] = 1

    if curr_y == G.shape[1] - 1:
        return True

    if G[curr_x, curr_y+1] == 0 and visited[curr_x, curr_y+1] == 0:
        if is_percolating_rec(G, curr_x,curr_y+1, visited):
            return True
    else:
        x = curr_x
        while x >= 1 and G[x-1, curr_y] == 0:
            x -= 1
            if G[x, curr_y+1] == 0 and visited[x, curr_y+1] == 0:
                if is_percolating_rec(G, x, curr_y+1, visited):
                    return True

        x = curr_x
        while x < G.shape[0]-1 and G[x+1, curr_y] == 0:
            x += 1
            if G[x, curr_y+1] == 0 and visited[x, curr_y+1] == 0:
                if is_percolating_rec(G, x, curr_y+1, visited):
                    return True

    return False

def is_percolating(G):
    """
    Checks if the grid is percolating.

    Args:
        G: The grid.

    Returns:
        True if the grid is percolating, False otherwise.
    """
    for x in range(G.shape[0]):
        if is_percolating_rec(G, x, 0):
            return True

    return False

def print_grid(G):
    """
    Prints the grid.

    Args:
        G: The grid.
    """
    for y in range(G.shape[1]):
        for x in range(G.shape[0]):
            if G[x, y] == 0:
                print("\u2591", end="")
            else:
                print("\u2588", end="")
        print("")

def random_grid(n, p=0.5):
    """
    Returns a random grid of size n, where each cell is permuable with probability p.

    Args:
        n: The size of the grid.
        p: The probability of a cell being open.

    Returns:
        The random grid.
    """
    grid = np.random.random(size=(n, n))
    grid = grid > p

    return grid

def estimate_percolating(n, p, num=1000):
    """
    Estimates the percolation rate for a grid of size n with probability p.

    Args:
        n: The size of the grid.
        p: The probability of a cell being open.
        num: The number of random grids to generate.

    Returns:
        The estimated percolation rate.
    """
    perc = 0
    for _ in range(num):
        grid = random_grid(n, p)
        perc += is_percolating(grid)

    return perc/num

def main():
    """
    Estimates the percolation rate for n=10,20,50 and p=0, 0.1, 0.2, .., 0.9, 1.0. It then displays the result in a matplotlib graph.
    """

    # Define the range of values for n and p.
    n_values = [10, 20, 50]
    p_values = np.arange(0, 1.05, 0.1)

    # Create figure
    fig, ax = plt.subplots()
    ax.set_xlabel("p")
    ax.set_ylabel("Percolation Probability")


    # Iterate over the n and p values.
    for n in n_values:
        # Create a list to store the percolation rates for each n and p value.
        percolation_rates = []
        for p in p_values:
            # Estimate the percolation rate.
            percolation_rate = estimate_percolating(n, p, num=1000)

            # Add the percolation rate to the list.
            percolation_rates.append(percolation_rate)

            print(percolation_rates)

        # Plot the percolation rates.
        ax.plot(p_values, percolation_rates, label=f"$n = {n}$")
        print(f"finished calculating n = {n}")

    ax.legend()
    fig.tight_layout()
    plt.savefig("exercise01_3c.png", dpi=200)
    plt.show()

if __name__ == "__main__":
    main()
